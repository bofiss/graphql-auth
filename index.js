import express from 'express';
import bodyParser from 'body-parser';

import { graphiqlExpress, graphqlExpress } from 'graphql-server-express';
import { makeExecutableSchema } from 'graphql-tools';
import cors from 'cors'

import typeDefs from './schema';
import resolvers from './resolvers';
import models from './models';
import jwt from 'jsonwebtoken';

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});
const SECRET =" arghjopkjkmlp146976544gdtihpjupijlkgdjk"
const app = express()
const addUser = async (req) => {
  const token = req.headers.authorization
  try {
    const { user } = await jwt.verify(token, SECRET)
    req.user = user
  }catch (err) {
    console.log(err)
  }
   req.next()
}

app.use(cors('*'))
app.use(addUser)
app.use(
  '/graphiql',
  graphiqlExpress({
    endpointURL: '/graphql',
  }),
);

app.use(
  '/graphql',
  bodyParser.json(),
  graphqlExpress(req => ({ schema, context: { user: req.user, models, SECRET } })),
);

models.sequelize.sync().then(() => app.listen(3000));
