import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import _ from 'lodash'
export default {
  User: {
    boards: ({ id }, args, { models }) =>
      models.Board.findAll({
        where: {
          owner: id,
        },
      }),
    suggestions: ({ id }, args, { models }) =>
      models.Suggestion.findAll({
        where: {
          creatorId: id,
        },
      }),
  },
  Board: {
    suggestions: ({ id }, args, { models }) =>
      models.Suggestion.findAll({
        where: {
          boardId: id,
        },
      }),
  },
  Suggestion: {
    creator: ({ creatorId }, args, { models }) =>
      models.User.findOne({
        where: {
          id: creatorId,
        },
      }),
  },
  Query: {
    allUsers: (parent, args, { models }) => models.User.findAll(),
    me: (parent,  args , { models, user }) => {
      if (user){
        console.log("is logged in")
        return models.User.findOne({
          where: {
            id: user.id
          },
        })
      }else {
        console.log("is not logged in")
      }
      return null
    },
    userBoards: (parent, { owner }, { models }) =>
      models.Board.findAll({
        where: {
          owner,
        },
      }),
    userSuggestions: (parent, { creatorId }, { models }) =>
      models.Suggestion.findAll({
        where: {
          creatorId,
        },
      }),
  },

  Mutation: {

    updateUser: (parent, { username, newUsername }, { models }) =>
      models.User.update({ username: newUsername }, { where: { username } }),
    deleteUser: (parent, args, { models }) =>
      models.User.destroy({ where: args }),
    createBoard: (parent, args, { models }) => models.Board.create(args),
    createSuggestion: (parent, args, { models }) =>
      models.Suggestion.create(args),
    register: async (parent, args, { models }) => {
      const user = args;
      let salt = bcrypt.genSaltSync(12);
      user.password = bcrypt.hashSync(user.password, salt);
      return models.User.create(user)
    },
    login: async (parent, {username, password}, { models, SECRET }) => {
      const user = await models.User.findOne({where: {username}})
      if(!user){
        throw new error('No user with that username!')
      }
      if(!bcrypt.compareSync(password, user.password)) {
        throw new error('Incorrect password!')
      }
      const token = jwt.sign(
      {
          user: _.pick(user, ['id', 'username'])
       },
       SECRET,
       {
        expiresIn: '1y'
       }
     )
      return token
    }
  }
};
